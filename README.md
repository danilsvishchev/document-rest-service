# Document

Сервис для создания документов

## Техническое задание

Требуется разработать REST микросервис используя Spring Framework, который будет принимать 1 документ вида:

{
"seller":"123534251",
"customer":"648563524",
"products":[{"name":"milk","code":"2364758363546"},{"name":"water","code":"3656352437590"}]
}

В данном документе все поля обязательны. Идентификатор seller/customer - строка 9 символов,
идентификатор продукта - строка 13 символов. 

В случае, если документ не корректен, возвращать сообщения об ошибках в документе.

## Описание

В контроллере DocumentController 
```
document-rest-service/src/main/java/com/ekviron/document/controller
```
представлен end-point для создания документа в соответствии с техническим заданием:
```
http://localhost:8080/api/v1/document
```

Техническая документация:
```
http://localhost:8080/swagger-ui.html
```

Тестирование данного end-point'a выполнено в классе TestDocumentController:
```
document-rest-service/src/test/java/com/ekviron/document/controller
```

## Выполнено с использованием

* [Spring Framework](https://spring.io/)
* [Swagger](https://swagger.io/)
* [PostgreSQL](https://www.postgresql.org/)
* [Flyway](https://flywaydb.org/)
* [JUnit](https://junit.org/junit5/)
* [Maven](https://maven.apache.org/)

## Автор

* **Danil Svishchev** - [Document](https://gitlab.com/danilsvishchev/document-rest-service)

