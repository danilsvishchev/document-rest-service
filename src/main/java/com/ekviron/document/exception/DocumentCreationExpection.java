package com.ekviron.document.exception;

import lombok.Getter;

@Getter
public class DocumentCreationExpection extends Exception{
    private int code;

    public DocumentCreationExpection(int code, String message) {
        super(message, null, true, false);
        this.code = code;
    }
}
