package com.ekviron.document.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DocumentRequest {
    @NotNull
    @Size(max = 9)
    private String seller;
    @NotNull
    @Size(max = 9)
    private String customer;
    @NotNull
    private List<ProductRequest> products;
}
