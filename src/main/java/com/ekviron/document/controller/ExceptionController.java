package com.ekviron.document.controller;

import com.ekviron.document.dto.ErrorResponse;
import com.ekviron.document.exception.DocumentCreationExpection;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(value = DocumentCreationExpection.class)
    public ResponseEntity<ErrorResponse> entityNotFoundException(DocumentCreationExpection exception) {
        return ResponseEntity.status(exception.getCode()).body(new ErrorResponse("Ошибка создания документа", exception.getMessage(), exception.getCode()));
    }
}
