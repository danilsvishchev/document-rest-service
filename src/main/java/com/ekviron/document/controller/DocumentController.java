package com.ekviron.document.controller;

import com.ekviron.document.dto.DocumentRequest;
import com.ekviron.document.dto.DocumentResponse;
import com.ekviron.document.exception.DocumentCreationExpection;
import com.ekviron.document.model.Document;
import com.ekviron.document.repository.DocumentRepository;
import com.ekviron.document.service.DocumentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/document")
public class DocumentController {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<DocumentResponse>> findAllDocuments() {
        return ResponseEntity.ok(documentService.findAllDocuments());
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<DocumentResponse> addDocument(@Valid @RequestBody DocumentRequest request, BindingResult result) throws DocumentCreationExpection {
        if (result.hasErrors()){
            throw new DocumentCreationExpection(HttpStatus.BAD_REQUEST.value(), "Произошла ошибка, проверьте корретность введеных данных");
        }
        return ResponseEntity.ok(documentService.addDocument(request));
    }
}
