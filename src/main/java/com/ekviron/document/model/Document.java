package com.ekviron.document.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="document")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Version
    private Long version;
    @Column
    private String seller;
    @Column
    private String customer;
    @OneToMany(mappedBy = "document", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Product> products;
}
