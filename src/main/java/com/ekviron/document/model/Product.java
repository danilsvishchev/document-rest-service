package com.ekviron.document.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="product")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    private String code;
    @Version
    private Long version;
    @Column
    private String name;
    @ManyToOne (optional=false, cascade=CascadeType.ALL)
    @JoinColumn(name = "document_id")
    private Document document;
}
