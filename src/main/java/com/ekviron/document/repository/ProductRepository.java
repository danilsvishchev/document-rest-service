package com.ekviron.document.repository;

import com.ekviron.document.model.Document;
import com.ekviron.document.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String> {
}
