package com.ekviron.document.service;

import com.ekviron.document.dto.ProductRequest;
import com.ekviron.document.model.Document;
import com.ekviron.document.model.Product;
import com.ekviron.document.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> addProductsForDocument(List<ProductRequest> productRequest, Document document){
        List<Product> products = new ArrayList<>();
        for (ProductRequest rq : productRequest){
            Product product = new Product();
            product.setCode(rq.getCode());
            product.setName(rq.getName());
            product.setDocument(document);
            productRepository.save(product);
            products.add(product);
        }
        return products;
    }
}
