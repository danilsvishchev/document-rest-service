package com.ekviron.document.service;

import com.ekviron.document.config.ListMapper;
import com.ekviron.document.dto.DocumentRequest;
import com.ekviron.document.dto.DocumentResponse;
import com.ekviron.document.exception.DocumentCreationExpection;
import com.ekviron.document.model.Document;
import com.ekviron.document.repository.DocumentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class DocumentService {
    private final ProductService productService;
    private final ModelMapper modelMapper;
    private final ListMapper listMapper;
    private final DocumentRepository documentRepository;

    public DocumentService(ProductService productService, ModelMapper modelMapper, ListMapper listMapper, DocumentRepository documentRepository) {
        this.productService = productService;
        this.modelMapper = modelMapper;
        this.listMapper = listMapper;
        this.documentRepository = documentRepository;
    }

    public List<DocumentResponse> findAllDocuments(){
        return listMapper.mapList(documentRepository.findAll(), DocumentResponse.class);
    }

    public DocumentResponse addDocument(DocumentRequest documentRequest) throws DocumentCreationExpection {
        if (StringUtils.isEmpty(documentRequest.getCustomer()) || StringUtils.isEmpty(documentRequest.getSeller()) ||
                CollectionUtils.isEmpty(documentRequest.getProducts())){
            throw new DocumentCreationExpection(HttpStatus.BAD_REQUEST.value(), "Проверьте корректность введенных данных");
        }
        Document document = new Document();
        DocumentResponse documentResponse = new DocumentResponse();
        document.setSeller(documentRequest.getSeller());
        document.setCustomer(documentRequest.getCustomer());
        document.setProducts(productService.addProductsForDocument(documentRequest.getProducts(), document));
        documentRepository.save(document);
        modelMapper.map(document, documentResponse);
        return documentResponse;
    }
}
