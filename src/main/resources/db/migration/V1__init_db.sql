create table document (
    id serial not null primary key,
    version int8,
    seller varchar(9) not null,
    customer varchar(9) not null
    );

create table product (
    code varchar(13) not null primary key,
    version int8,
    name varchar(255) not null,
    document_id int4 not null
    );

alter table if exists product
    add constraint product_document_fk
    foreign key (document_id) references document;