package com.ekviron.document.controller;

import com.ekviron.document.repository.DocumentRepository;
import com.ekviron.document.repository.ProductRepository;
import com.ekviron.document.service.DocumentService;
import com.ekviron.document.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
public class TestDocumentController {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    DocumentController documentController;
    @MockBean
    private DocumentRepository documentRepository;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private DocumentService documentService;
    @MockBean
    private ProductService productService;

    @Test
    public void addCorrectDocument() throws Exception{
        String document = "{\"seller\":\"123534251\",\"customer\":\"648563524\",\"products\":[{\"name\":\"milk\",\"code\":\"2364758363546\"},{\"name\":\"water\",\"code\":\"3656352437590\"}]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/document")
                .content(document)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void addUncorrectDocument() throws Exception{
        String document = "{\"seller\": null,\"customer\":\"648563524\",\"products\":[{\"name\":\"milk\",\"code\":\"2364758363545\"},{\"name\":\"water\",\"code\":\"3656352437591\"}]}";
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/document")
                .content(document)
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
